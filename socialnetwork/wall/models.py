from django.db                  import models
from django.contrib.auth.models import User

from datetime import datetime

class Profile(models.Model):
    user          = models.OneToOneField(User, on_delete=models.CASCADE)
    profile_photo = models.ImageField(upload_to='profile_pic', null=True, blank=True)

    def __str__(self):
        return self.user.username

class Post(models.Model):
    author       = models.ForeignKey(Profile, on_delete=models.CASCADE, related_name='postauthor')
    title        = models.CharField(max_length=100)
    text         = models.TextField()
    post_photo   = models.ImageField(upload_to='post_photo', null=True, blank=True)
    created_date = models.DateTimeField(default=datetime.now)

    class Meta:
        verbose_name_plural = "Posts"

    def __str__(self):
        return self.text

    def get_absolute_url(self):
        return reverse('post_detail', args=[str(self.id)])

    def get_comments(self):
        return self.comment_set.all()

class Comment(models.Model):
    author        = models.ForeignKey(Profile, on_delete=models.CASCADE, related_name='commentauthor')
    post          = models.ForeignKey(Post, on_delete=models.CASCADE)
    title         = models.CharField(max_length=340)
    text          = models.TextField()
    comment_photo = models.ImageField(upload_to='comment_photo', null=True, blank=True)
    created_date  = models.DateTimeField(default=datetime.now)

    def __str__(self):
        return self.text

