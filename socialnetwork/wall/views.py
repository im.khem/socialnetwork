from django.shortcuts import render
from .models          import Profile, Post, Comment

def index(request):
    return render(request, 'index.html')
